#!/bin/bash

DFN_PKCS12=$1
CERT_STORE=$HOME/.ufz-cert

if [[ $1 == "-h" || $1 == "" ]]; then
    cat <<EOF
ERROR: No certificate file provided!

Usage:

./split-certificate.sh [/path/to/certificate.p12]

The output files (private key and certificate) will be written to '~/.ufz-cert'

EOF
    exit 1
fi

[[ ! -d $CERT_STORE ]] && echo "creating keychain..." && mkdir $CERT_STORE

echo -n "Enter certificate password: "
read -s certkey

echo "extracting certificate..."
openssl pkcs12 -passin pass:$certkey -in $DFN_PKCS12 -out $CERT_STORE/dfn-cert-$(date '+%Y').pem -nokeys -legacy
echo "extracting private key..."
openssl pkcs12 -passin pass:$certkey -in $DFN_PKCS12 -passout pass:$certkey -out $CERT_STORE/dfn-privkey-$(date '+%Y').key -legacy
echo "done!"
