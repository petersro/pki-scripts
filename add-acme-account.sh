#!/usr/bin/env bash

if [ "$(which jq)" = "jq not found" ]; then
    echo "ERROR: jq not found, please install it and try again"
    exit 1
fi

sectigoUsername=$(security find-generic-password -l sectigo-rao-account | grep acct | awk -F '"' '{print $4}')
sectigoPassword=$(security find-generic-password -l sectigo-rao-account -w)

function addNewACMEAccount {
    echo
    echo "adding ACME account for $name..."
    ACMELocation=$(curl 'https://cert-manager.com/api/acme/v1/account' -s -i -X POST -H "login: $sectigoUsername"  -H "password: $sectigoPassword" -H "Content-Type: application/json;charset=utf-8" -H "customerUri: DFN" -d "{\"name\":\"$name\",\"acmeServer\":\"https://acme.sectigo.com/v2/OV\",\"organizationId\":28876,\"evDetails\":{\"orgName\":\"Helmholtz-Zentrum für Umweltforschung GmbH - UFZ\"}}" | grep Location | awk -F '/' '{print $NF}' | tr -d '[:cntrl:]')
    echo
    echo "The Account ID is: $ACMELocation"

    accountDetails=($(curl "https://cert-manager.com/api/acme/v1/account/$ACMELocation" -s -X GET -H "login: $sectigoUsername"  -H "password: $sectigoPassword" -H "Content-Type: application/json;charset=utf-8" -H "customerUri: DFN" | jq '.macId, .macKey' | awk -F '"' '{print $2}'))
    hmacId=$(echo ${accountDetails[0]})
    hmacKey=$(echo ${accountDetails[1]})
    echo
    echo "Key ID: $hmacId"
    echo "HMAC Key: $hmacKey"

}

function addDomainsToACMEAccount {
    echo
    echo "adding domains $domains..."
    domainstring=$(echo $domains | tr " " "\n" | jq --compact-output -nR '{domains: [inputs | {name: .}]}')
    curl "https://cert-manager.com/api/acme/v1/account/$ACMELocation/domains" -s -X POST -H "login: $sectigoUsername"  -H "password: $sectigoPassword" -H "Content-Type: application/json;charset=utf-8" -H "customerUri: DFN" -d "$domainstring" 
}

function addSectigoDomain {
    echo
    echo "adding domain $newDomain..."
    curl 'https://cert-manager.com/api/domain/v1' -s -X POST -H "login: $sectigoUsername"  -H "password: $sectigoPassword" -H "Content-Type: application/json;charset=utf-8" -H "customerUri: DFN" -d "{\"name\":\"$newDomain\",\"active\":true,\"delegations\":[{\"orgId\": \"28876\",\"certTypes\":[\"SSL\"]}]}"
}

while getopts hn:d: arg
do
    case $arg in
        d) domains+=("$OPTARG")
        for newDomain in ${domains[@]}; do
            addSectigoDomain
        done
        addDomainsToACMEAccount
	   exit 0
           ;;
        n) name="$OPTARG"
	    addNewACMEAccount
           ;;
        h) echo "usage: add-acme-account [-n <account-name>] [-d <domains-to-add>] (-h)"
	   echo "-n: specify name for the ACME account"
	   echo "-d: set domains to associate to the new account - multiple domains possible, separated by whitespace"
       echo "    if a domain is not already available, it will be created"
	   echo "-h: display this help"
           ;;
        *) echo "no or incorrect options provided, see help (-h)"
	   exit 1
           ;;
    esac
done
