# UFZ-CA Scripts

This repository contains useful scripts for handling certificates.

# split-certificate.sh

Used to split exported p12-certificate into pem and key for use with Linux VPN client.

Usage:

```
$ git clone https://git.ufz.de/petersro/pki-scripts.git
$ cd pki-scripts/
$ ./split-certificate.sh /path/to/certificate.p12
```

The files will be written to `~/.ufz-cert`. If this directory doesn't exist, it will be created.

# add-acme-account.sh

Used to add a new ACME account to the Sectigo CA.

Usage:

Create a new item in macOS Keychain with name `sectigo-rao-account` and your Sectigo RAO credentials.
If you're not using macOS, adjust the variables `sectigoUsername` and `sectigoPassword` in the script!

Then, you're good to go:

```
$ ./add-acme-account.sh -n "<your desired name for the account>" -d "domain1.ufz.de domain2.ufz.de domainN.ufz.de"

```

Output will be like that:

```
$ ./add-acme-account.sh -n "Test (Robin Peters)" -d "aaatest.ufz.de epsdev.intranet.ufz.de"
adding ACME account for Test (Robin Peters)...
create new ACME account...

The Account ID is: 9323

Key ID: M8GstqX2_EPufTHFZVR7LA
HMAC Key: tXl-OGrGNM99Fofp7ybCZlhm87WcyxxaOkfF3RM5bAYeTlM5UrWua-4RSxn1fuj48Rto-HtaxafSMagVNfNxtQ

adding domain aaatest.ufz.de...
adding domain epsdev.intranet.ufz.de...
{}

```